#My zsh and Vim settings
---

## Overview

###zsh  
- プラグイン管理はzplug

- oh-my-zshを自分で管理してる感じ

- オリジナルschemeは.dotfiles/.zsh/themes/majesty.zsh-theme

###Vim  
- プラグイン管理はVundle

- オリジナルschemeは.dotfiles/.vim/colors/majesty.vim


## Description
.zshrcの動作は以下。

1.  プラグインのインストール  

2. zshの機能設定  

3. ~/.zsh/lib 内の設定ファイルをインポート（多分2と結構被ってるから2があまり必要ないかも）  

4. テーマをインポート。$ZSH_THEMEの値を変えるだけで好きなテーマに出来る。  

5. その他環境変数やエイリアスの設定  

## Requirement
必要に応じてインストール

- zsh  
  ```
  $ brew install zsh
  ```
  (元々zshはインストールされてるけどこれで最新バージョンに）

    - ログインシェルの設定  
    ```
    $ sudo vim /etc/shells
    ```  
    して最終行に `/usr/local/bin/zsh` を追加。  
    そしてログインシェル変更。  
    ```
    $ chsh -s /usr/local/bin/zsh
    ```

- zplug  
  ```
  $ brew install zplug
  ```

- Vundle  
  ```
  $ git clone http://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
  ```

- iTerm2
    - タブ・ペイン分割がデフォルトで出来る上、ショートカットや色が細かく設定出来る。
    - schemeをjapanesqueとかにすると色が丁度よくなるはず。

##Install
- 参考にして使う場合（推奨?）  
.zshrc等を切り貼りしてご自由に。 


- そのままインストールする場合  
どの環境でも動くか検証してないのでエラーが出たりしたらごめんなさい。

1. リポジトリをホームディレクトリにクローン  
  ```
  $ cd
  ```   
  ```
  $ git clone https://kuritti623@bitbucket.org/kuritti623/.dotfiles.git
  ```

2. .zshrc後半の設定（#Vim 以下）を削除 or コメントアウトする  

3. （ある場合は）.zsh/, .zshrc, .vim/, .vimrcのバックアップ  

4. - git管理しない場合は各ファイルをホームに。  
    ```
    $ mv .zshrc .zsh .vimrc .vim ~/
    ```

    - git管理する場合は設定ファイルを~/.dotfilesに置くのでリンクを張る  
    ```
    $ ln -s ~/.dotfiles/.zsh ~/.zsh
    ```  
    ```
    $ ln -s ~/.dotfiles/.zshrc ~/.zshrc
    ```  
    ```
    $ ln -s ~/.dotfiles/.vimrc ~/.vimrc
    ```  
    ```
    $ ln -s ~/.dotfiles/.vim/colors ~/.vim/colors  
    ```  
    (~/.vim/bundle/ 以下にgitリポジトリがあるので~/.vim/colorsのみ共有)

5. ソースする  
  ```
  $ source ~/.zshrc
  ```

6. vimのプラグインをインストール  
  ```
  $ vim
  ```  
  ```
  :PluginInstall
  ```
