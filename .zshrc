# zplug init
export ZPLUG_HOME=/usr/local/opt/zplug
source $ZPLUG_HOME/init.zsh

# Path to zsh setting files
export ZSH=/Users/taisei/.zsh

##############################

# plugins
# auto completion
#zplug "zsh-users/zsh-autosuggestions"

# command completion
zplug "zsh-users/zsh-completions"

# completion from history
zplug "zsh-users/zsh-history-substring-search"

# syntax highlight
#zplug "zsh-users/zsh-syntax-highlighting", defer:2

# terminal environment with 256 colors
#zplug "chrissicool/zsh-256color"

# git command aliases
zplug "plugins/git", from:oh-my-zsh

# git prompt
#zplug "plugins/git-prompt", from:oh-my-zsh

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load #--verbose

##############################

# zsh option

# environmental variable
export LANG=ja_JP.UTF-8

# color
autoload -Uz colors
colors

# completion
# enable completion
autoload -Uz compinit
compinit

# completion using capital or small letter
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# no complementing current directory after ../
zstyle ':completion:*' ignore-parents parent pwd ..

# completion after sudo command
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                   /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin

# ps command completion
zstyle ':completion:*:processes' command 'ps x -o pid,s,args'

# completion list
setopt auto_list

# auto completion in turn
setopt auto_menu

# history
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

# share history between zsh
setopt share_history

# ignore same command
setopt hist_ignore_all_dups

# ignore command starts from space
setopt hist_ignore_space

# reduce blanks
setopt hist_reduce_blanks

# japanese
setopt print_eight_bit

# no beep
setopt no_beep

# disable flow control
setopt no_flow_control

# Ctrl+D
setopt ignore_eof

# regard '#' as comment
setopt interactive_comments

# change directory using only directory name
setopt auto_cd

# expanding wildcard characters
setopt extended_glob

##############################

# Load all of the config files in ~/.zsh/ that end in .zsh
for config_file ($ZSH/lib/*.zsh); do
  #custom_config_file="${ZSH_CUSTOM}/lib/${config_file:t}"
  #[ -f "${custom_config_file}" ] && config_file=${custom_config_file}
  source $config_file
done

# Theme setting

# Load the theme
ZSH_THEME="majesty"
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

if [[ "$ZSH_THEME" == "random" ]]; then
  if [[ "${(t)ZSH_THEME_RANDOM_CANDIDATES}" = "array" ]] && [[ "${#ZSH_THEME_RANDOM_CANDIDATES[@]}" -gt 0 ]]; then
    themes=($ZSH/themes/${^ZSH_THEME_RANDOM_CANDIDATES}.zsh-theme)
  else
    themes=($ZSH/themes/*zsh-theme)
  fi
  N=${#themes[@]}
  ((N=(RANDOM%N)+1))
  RANDOM_THEME=${themes[$N]}
  source "$RANDOM_THEME"
  echo "[oh-my-zsh] Random theme '$RANDOM_THEME' loaded..."
else
  if [ ! "$ZSH_THEME" = ""  ]; then
    if [ -f "$ZSH_CUSTOM/$ZSH_THEME.zsh-theme" ]; then
      source "$ZSH_CUSTOM/$ZSH_THEME.zsh-theme"
    elif [ -f "$ZSH_CUSTOM/themes/$ZSH_THEME.zsh-theme" ]; then
      source "$ZSH_CUSTOM/themes/$ZSH_THEME.zsh-theme"
    else
      source "$ZSH/themes/$ZSH_THEME.zsh-theme"
    fi
  fi
fi

##############################

# Vim
if [ -f /Applications/MacVim.app/Contents/MacOS/Vim ]; then
  alias vi='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/Vim "$@"'
  alias vim='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/Vim "$@"'
fi

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

#rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

alias be="bundle exec"

# GNU command
export PATH=/usr/local/opt/coreutils/libexec/gnubin:${PATH}
export MANPATH=/usr/local/opt/coreutils/libexec/gnuman:${MANPATH}

# my alias
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias ls="ls -F --color=auto"
alias l="ls -lahF"
alias ll="ls -lhF"
alias la="ls -lAhF"
alias lsa="ls -lahF"
alias mkdir="mkdir -p"

alias zshconfig="vim ~/.zshrc"
alias zshrc="source ~/.zshrc"
alias vimconfig="vim ~/.vimrc"


# application alias
alias chrome="open -a Google\ Chrome"
alias inkscape="open -a Inkscape.app"
#export PATH=$PATH:/Applications/Inkscape.app/Contents/Resources/bin
alias mail="open -a Thunderbird"
alias line="open -a LINE"
alias klayout="open -a klayout"
alias eagle="open -a /Applications/EAGLE-8.6.3/EAGLE.app"
alias tuxguitar="open -a tuxguitar"
alias atom="open -a Atom"
alias ltspice="open -a LTspice"
alias mendeley="open -a Mendeley"
alias vnc="open -a VNC\ Viewer"
alias cake="open -a Cakebrew"
alias st="open -a Sourcetree"
alias skim="open -a Skim"
alias filezilla="open -a FileZilla"
alias airstation="open -a BUFFALO_AirSet"
alias topcoder="javaws ~/Work/TopCoder/ContestAppletProd.jnlp"
alias excel="open -a Microsoft\ Excel"
alias word="open -a Microsoft\ Word"
alias power="open -a Microsoft\ PowerPoint"
alias slack="open -a Slack"
alias skype="open -a Skype"
